const Discord = require("discord.js");
const settings = require("../settings");
const sendRichEmbed = require("../src/sendRichEmbed");

/**
 * @param {Discord.Client} client
 * @param {Array<Discord.GuildMember>} member
 */
module.exports.call = (client, member) => {
    member = member[0];
    sendRichEmbed(client.guilds.find(g => g.id == settings.mainServerID).channels.find(c => c.id == settings.errorCatcher.logChannelID), "join", [member.user.tag]);
    /*DEBUG*/ client.guilds.find(g => g.id == settings.mainServerID).channels.find(c => c.id == settings.errorCatcher.logChannelID).send(`-> ${member.user.tag} joined the server`);
};