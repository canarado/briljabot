// settings file for all commands of BriljaBot

const settings = require("./settings");

module.exports = {
    yikes: {
        deleteMessage: true,            // whether or not to delete the user's message on execution
        filePaths: [                    // array with paths to the "yikes" image files (relative to root)
            "./images/yikes1.jpg",
            "./images/yikes2.jpg"
        ]
    },
    angery: {
        deleteMessage: true,            // whether or not to delete the user's message on execution
        filePaths: [                    // array with paths to the "angery" image files (relative to root)
            "./images/angery.png"
        ]
    }
}