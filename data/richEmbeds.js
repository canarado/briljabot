const settings = require("../settings");

module.exports = {
    startup: {
        title: "♻️ I just started up",
        fields: [
            {
                title: "Commands:",
                content: "{{0}}",
                inline: true
            },
            {
                title: "Events:",
                content: "{{1}}",
                inline: true
            }
        ],
        color: settings.richEmbed.colors.good
    },
    join: {
        title: "▶ A user joined the server",
        fields: [
            {
                title: "User:",
                content: "{{0}}",
                inline: false
            }
        ],
        color: settings.richEmbed.colors.default
    },
    leave: {
        title: "❌ A user has left the server",
        fields: [
            {
                title: "User:",
                content: "{{0}}",
                inline: false
            }
        ],
        color: settings.richEmbed.colors.warn
    },
    agree: {
        title: "✅ A user agreed to the rules",
        fields: [
            {
                title: "User:",
                content: "{{0}}",
                inline: false
            }
        ],
        color: settings.richEmbed.colors.good
    }
}