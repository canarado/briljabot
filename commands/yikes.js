const Discord = require("discord.js");
const cmdSet = require("../commandSettings");
const jsl = require("svjsl");

module.exports.info = {
    perms: 0,
    desc: "Sends an image in case of a yikes-emergency",
    where: "any"
};

module.exports.run = (client, message, args) => {
    if(cmdSet.yikes.deleteMessage) message.delete();

    let randNum = jsl.randRange(0, cmdSet.yikes.filePaths.length);
    let filePath = cmdSet.yikes.filePaths[randNum];

    message.channel.send({
        file: filePath
    });
};