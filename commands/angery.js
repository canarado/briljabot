const Discord = require("discord.js");
const cmdSet = require("../commandSettings");
const jsl = require("svjsl");

module.exports.info = {
    perms: 0,
    desc: "Use this command when you're  ***a n g e r y***",
    where: "any"
};

module.exports.run = (client, message, args) => {
    if(cmdSet.angery.deleteMessage) message.delete();

    let randNum = jsl.randRange(0, cmdSet.angery.filePaths.length);
    let filePath = cmdSet.angery.filePaths[randNum];

    message.channel.send({
        file: filePath
    });
};