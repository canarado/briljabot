const Discord = require("discord.js");


module.exports.info = {
    perms: 2,
    desc: "Restarts ToS-Bot",
    where: "any"
};

module.exports.run = (client, message, args) => {
    message.react("✅").then(() => {
        setTimeout(() => {
            message.delete().then(() => {
                process.exit(2);
            });
        }, 750);
    });
};