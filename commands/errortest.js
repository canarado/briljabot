const Discord = require("discord.js");
const catchError = require("../src/errorCatcher");
const jsl = require("svjsl");


module.exports.info = {
    perms: 0,
    desc: "Tests out the error catcher module",
    where: "any"
};

module.exports.run = (client, message, args) => {
    message.delete();
    return catchError("commands/errortest.js", "This is just a test error - current time: " + new Date().toUTCString() + "\nRandom Number: " + jsl.randRange(1, 1000) + "\n\nArgs: " + args.join(", "));
};