const fs = require("fs");
const settings = require("../settings");
const getDate = require("./getDate");
const jsl = require("svjsl");
const catchError = require("./errorCatcher");


/**
 * 
 * @param {String} fileName The log file name without the .log extension
 * @param {String} title The title of the logged line or the content if no separate content value is passed
 * @param {String} [content] The content of the logged line - leave empty to have a single logged line with only the title value 
 */
const logger = (fileName, title, content) => {
    try {
        let final = "";
        let filePath = `${settings.logger.basePath}${fileName}.log`;

        if(settings.logger.timestamp === true) final = getDate();

        if(!jsl.isEmpty(content)) {
            final += ` ${title}:\n`;

            final += `${content}\n\n`;
        }
        else {
            final += ` ${title}\n\n`;
        }

        if(fs.existsSync(filePath)) fs.appendFileSync(filePath, final);
        else fs.writeFileSync(filePath, final);
    }
    catch(err) {
        catchError(`Error while logging to file ${fileName}.log`, err, false);
    }
};

module.exports = logger;