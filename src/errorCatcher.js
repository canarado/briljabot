const fs = require("fs");
const jsl = require("svjsl");
const Discord = require("discord.js");
const settings = require("../settings");
const logger = require("./logger");
const col = require("./consoleColors");
const bot = require("./bot");


/**
 * Catches an error and logs it to the server's bot-logs channel
 * @param {String} file The file where the error originated from
 * @param {String} error The actual error message
 * @param {Boolean} [logError=false] Whether to log the error to a log file or not
 */
const errcatch = (file, error, logError) => {
    let client = bot.client;
    if(!jsl.isEmpty(client)) {
        let server = client.guilds.find(guild => guild.id == settings.mainServerID);
        let logChannel = server.channels.find(channel => channel.id == settings.errorCatcher.logChannelID);

        let errEmbed = new Discord.RichEmbed()
            .setTitle(`**‼ Error**`)
            .addField(`**File:**`, file, true)
            .addField(`**Error Message:**`, error.toString(), true)
            .setColor(settings.richEmbed.colors.error);

        logChannel.send(errEmbed);

        if(logError === true) logger("error", `Error in "${file}"`, error);
    }
    else {
        console.log(`${col.red}Error while catching error (that irony tho), this is most likely because an error was trying to be caught while the client was not logged in yet.\nHere is the raw output:${col.rst}${col.yellow}\nFile: ${file}\nError: ${error}${col.rst}\n\n`);
        logger("error", `Error in "${file}"`, error);
        process.exit(1);
    }
};

module.exports = errcatch;