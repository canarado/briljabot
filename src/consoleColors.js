/**
 * Console color codes
 */
const colors = {
    rst: "\x1b[0m",
    reset: "\x1b[0m",
    fat: "\x1b[37m",
    red: "\x1b[31m\x1b[1m",
    green: "\x1b[32m\x1b[1m",
    yellow: "\x1b[33m\x1b[1m",
    blue: "\x1b[34m\x1b[1m",
    pink: "\x1b[35m\x1b[1m",
    cyan: "\x1b[36m\x1b[1m"
}

module.exports = colors;