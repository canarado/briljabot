const Discord = require("discord.js");
const settings = require("../settings");
const bot = require("./bot");

/**
 * Returns the permission level of the user
 * @param {Discord.User} member The member that should be checked
 * @returns {Number} Permission level (0-2, where 0 is lowest)
 */
const check = member => {
    let guild = bot.client.guilds.find(g => g.id == settings.mainServerID);
    let permLevel = 0;

    settings.perms.levels.forEach((l, i) => {
        try {
            let checkMember = guild.members.find(m => m.id == member.id);
            let checkRole = guild.roles.find(r => r.id == l);
            if(checkMember != null && checkMember.roles.has(checkRole.id)) permLevel = i + 1;
        }
        catch(err) {
            console.log(`\nERR WHILE CHECKING PERM LEVEL: ${err}\n`);
            return 0;
        }
    });

    return permLevel;
}

module.exports = check;