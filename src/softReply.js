const Discord = require("discord.js");
const catchError = require("./errorCatcher");


/**
 * Tries to DM the author of the provided message and if it fails, sends the reply in the channel of the provided message
 * @param {Discord.Message} message The message to reply to
 * @param {String} text The text to reply with
 * @param {Boolean} deleteMessage Whether or not to delete the message as well
 */
const softReply = (message, text, deleteMessage) => {
    if(deleteMessage === true) message.delete();

    try {
        message.author.send(text);
    }
    catch(err) {
        text = text.slice(0, 1).toLowerCase() + text.substring(1); // converts the first char to lowercase
        message.reply(text).catch(err2 => catchError("softReply.js", `Error while sending soft reply in channel <#${message.channel.id}>: ${err2}`, true));
    }
}

module.exports = softReply;