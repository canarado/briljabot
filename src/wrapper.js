const debuggerActive = typeof v8debug === "object" || /--debug|--inspect/.test(process.execArgv.join(" "));
const wrap = require("node-wrap");
const settings = require("../settings");
const col = require("./consoleColors");

console.log(`${col.yellow}Debugger active: ${debuggerActive}\n${col.cyan}${col.rst}`);


// Node-Wrap is not Debugger-friendly so it needs to be disabled when the debugger is active

if(debuggerActive !== true) {
    wrap(settings.mainFile, {
        bootLoopDetection: settings.wrapper.bootLoopDetection,
        console: settings.wrapper.logInfo,
        logFile: settings.wrapper.logFile,
        logTimestamp: true
    }, () => {
        // on starting up
    }, () => {
        // on crash
    });
}
else require("./bot");