const Discord = require("discord.js");
const settings = require("../settings");
const richEmbeds = require("../data/richEmbeds");


/**
 * Constructs a RichEmbed from the data/richEmbeds.js file and sends it to either the specified user or guild channel
 * @param {(Discord.User|Discord.GuildChannel)} userorchannel A user or channel to send the RichEmbed to
 * @param {String} name The name of the RichEmbed that gets loaded from the richEmbeds.js file
 * @param {Array} [args] Any other args that should be passed to the RichEmbed. These will replace the placeholders that are formatted like this: {{x}}, where x is the index of the array item - (0-indexed)
 */
const send = (userorchannel, name, args) => {
    new Discord.RichEmbed()

    if(userorchannel instanceof Discord.User)
    {
        // userorchannel is a user
    }
    else if(userorchannel instanceof Discord.GuildChannel)
    {
        // userorchannel is a channel
    }
}

module.exports = send;