const Discord = require("discord.js");
const jsl = require("svjsl");
const client = new Discord.Client();
const settings = require("../settings");
const fs = require("fs");
const catchError = require("./errorCatcher");
const col = require("./consoleColors");
const perms = require("./perms");
const logger = require("./logger");
const softReply = require("./softReply");
const sendRichEmbed = require("./sendRichEmbed");
const { shoukaku } = require("shoukaku");

require('dotenv').config(); // load .env file

var allCommands = [], allEvents = [];

// lavalink server information
const lavaLinkServer = [
    {
        name: "lavalinkServer",
        host: "localhost",
        port: process.env.LAVALINK_PORT,
        auth: process.env.LAVALINK_AUTH
    }
]

const Carrier = new shoukaku(client, {
    resumable: 'bbres',
    resumableTimeout: 30,
    reconnectTries: 2,
    restTimeout: 10000
})

Carrier.on('ready', (node) => {
    console.log(`LavaLink node ${node} is connected!`)
}).on('error', (name, error) => {
    console.log(`LavaLink node ${node} emitted an error`, error)
}).on('close', (name, code, reason) => {
    console.log(`Lavalink Node: ${name} closed with code ${code}. Reason: ${reason || 'No reason'}`)
}).on('disconnected', (name, reason) => {
    console.log(`Lavalink Node: ${name} disconnected. Reason: ${reason || 'No reason'}`)
})

client.on("ready", () => {
    console.log(`\n\n${col.green}[START]${col.rst} BriljaBot has started up`);
    console.log(`${col.green}─────────────────────────────────${col.rst}\n\n`);

    registerEvents(); // register all events inside the events folder (specified in the settings)

    registerCommands(); // register all commands inside the commands folder (specified in the settings)

    setInterval(()=>setRandomActivity(), settings.status.changeInterval * 60 * 1000); // new activity every x mins

    client.user.setAvatar(settings.status.iconURL).catch(err => {}); // set avatar

    Carrier.build(lavaLinkServer, { id: client.user.id })
});

/**
 * Registers all events inside the events folder (specified in the settings)
 */
function registerEvents() {
    let eventFiles = [];
    
    eventFiles = fs.readdirSync(settings.handler.eventsBasePath);
    eventFiles.forEach(event => allEvents.push(event.split(".")[0]));

    allEvents.forEach((event, i) => {
        try {
            let eventFilePath = `../${settings.handler.eventsBasePath}${eventFiles[i]}`;
            let eventFilePathFS = `${settings.handler.eventsBasePath}${eventFiles[i]}`;
            if(fs.existsSync(eventFilePathFS)) {
                client.on(event, (...args) => {
                    let eventFile = require(eventFilePath);
                    if(eventFile.call != undefined) eventFile.call(client, args);
                    else throw new Error(`call() function was not found (typeof "${typeof eventFile.call}" / call() equals: "${eventFile.call}"). Make sure it exists and is exported correctly.`);
                });
            }
            else throw new Error(`File "${eventFilePathFS}" not found. Direct fs output: "${fs.existsSync(eventFilePathFS)}"`);
        }
        catch(err) {
            catchError("src/bot.js", `Couldn't register event "${event}" due to error: ${err}`, true);
        }
    });
}

/**
 * Registers all commands inside the commands folder
 */
function registerCommands() {
    fs.readdirSync(settings.handler.commandsBasePath).forEach(file => { // get all available commands
        let absPath = `${settings.handler.commandsBasePath}${file}`;
        let relPath = `.${absPath}`;
        let reqFile = require(relPath);

        allCommands.push({
            name: file.replace(".js", ""),
            info: reqFile.info,
            absPath: absPath,
            relPath: relPath
        });
    });

    allCommands.forEach(cmd => {
        if(cmd.name.match(/[A-Z]+/gm)) return catchError("bot.js", `Command "${cmd.name}" contains uppercase letters`, true);
    });

    console.log(`${col.cyan}Found ${allCommands.length} command${allCommands.length == 1 ? "" : "s"}:${col.rst}`);
    allCommands.forEach((cmd, i) => {
        let color = "";
        switch(cmd.info.perms)
        {
            case 0:
                color = col.green;
            break;
            case 1:
                color = col.yellow;
            break;
            case 2:
                color = col.red;
            break;
            default:
                color = col.rst;
            break;
        }
        process.stdout.write(`${color}${cmd.name}${col.rst}${allCommands.length == i + 1 ? "" : ", "}`);
    });
    process.stdout.write("\n\n");
}

/**
 * Command Handler
 */
client.on("message", message => {
    if(message.author.bot) return;
    if(message.content.indexOf(settings.prefix) !== 0) return;

    let args = message.content.slice(settings.prefix.length).trim().split(/ +/g);
    let command = args[0].toLowerCase().replace(settings.prefix, "");
    args.shift();

    for(let i = 0; i < allCommands.length + 1; i++) {
        if(i == allCommands.length) {
            // no matching command was found
            if(settings.verbose) message.reply(`**!VERBOSE!**\n\nCommand not found!`);
            return logger("wrong-commands", `User "${message.author.tag}" tried to execute non-existing command "${command}"`);
        }
        else if(command == allCommands[i].name) {
            if(settings.verbose) message.reply(`**!VERBOSE!**\n\n**Command object:**\`\`\`json\n${JSON.stringify(allCommands[i], null, 4)}\`\`\``);

            let allowFurther = () => {
                let userPerms = perms(message.author);
                if(allCommands[i].info.perms <= userPerms) {
                    // user is granted access to the command
                    require(allCommands[i].relPath).run(client, message, args);
                }
                else {
                    // user is missing permissions to execute command
                    let permLevels = settings.perms.levels;
                    let curPerm = allCommands[i].info.perms - 1;
                    let roleName = message.guild.roles.find(r => r.id == permLevels[curPerm]).name;
                    return softReply(message, `You lack permissions to execute the \`${settings.prefix}${command}\` command.\nPlease ask someone with the role **@${roleName}** or higher to execute it for you.`, true);
                }
            };

            let where = allCommands[i].info.where;

            if(where == "dm" && message.guild == null) allowFurther();
            else if(where == "rules" && message.channel.id == settings.rulesChannelID) allowFurther();
            else if(where == "botplayground" && message.channel.id == settings.botPlaygroundID) allowFurther();
            else if(where == "any" && message.guild != null) allowFurther();
            else {
                let readableWhere = `any channel on the server except <#${settings.rulesChannelID}>`;
                switch(where) {
                    case "dm": readableWhere = "the DMs"; break;
                    case "rules": readableWhere = `the <#${settings.rulesChannelID}> channel`; break;
                    case "botplayground": readableWhere = `the <#${settings.botPlaygroundID}> channel`; break;
                    default: readableWhere = `ERROR, where=="${where}"`; break;
                }

                return softReply(message, `Please execute the \`${settings.prefix}${command}\` command only in ${readableWhere}.`, true);
            }

            return;
        }
    }


    // try {
    //     let commandFile = require(`.${settings.handler.commandsBasePath}${command}`);
    //     let info = commandFile.info;
    //     return commandFile.run(client, message, args);
    // } catch (err) {
    //     console.error(err);
    // }
});

function setRandomActivity() {
    let rand = jsl.randRange(0, (settings.status.activities.length - 1));

    let opts = {
        type: settings.status.activities[rand].type
    };

    if(!jsl.isEmpty(settings.status.activities[rand].url)) opts["url"] = settings.status.activities[rand].url;

    client.user.setActivity(settings.status.activities[rand].activity, opts);
}


// #MARKER DEBUG -- for testing only
if(settings.prodMode === false) {
    client.on("message", msg => {
        let msgc = msg.content.toLowerCase();
        if(msgc.startsWith("tperm")) msg.reply(`Your permission level is ${perms(msg.author)}`);
        if(msgc.startsWith("tgmadd")) require("../events/guildMemberAdd").call(client, msg.author);
    });
}







client.on("error", err => {
    if(!err.includes("ECONNRESET")) {
        console.log(`\n\n    ${col.red}[Client Error]: ${col.rst}${err.message}`);
        catchError("src/bot.js", `Client error event / ${err}`, true);
    }
});


console.log(`${col.yellow}Logging into the Discord API...${col.rst}\n\n`);

client.login(process.env.BOT_TOKEN).then(() => {
    let reArgs = [
        allCommands.length,
        allEvents.length
    ];
    let reName = "startup";
    sendRichEmbed(client.guilds.find(guild => guild.id == settings.mainServerID).channels.find(channel => channel.id == settings.errorCatcher.logChannelID), reName, reArgs);

    module.exports.client = client;
});