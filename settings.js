// Main settings file for BriljaBot

module.exports = {
    // dev options:
    prodMode: false, // if set to false, some developer only commands and features will be available
    verbose: false, // more verbose info on commands / console output


    prefix: "$",
    mainFile: "./src/bot.js",               // main script file that contains the command and event handler (path relative to root)
    mainServerID: "578267219000164363",     // the ID of the main server
    rulesChannelID: "578268039552958465",   // the ID of the rules channel
    botPlaygroundID: "578269289971449868",  // the ID of the bot-playground channel

    // status, profile picture and activities
    status: {
        iconURL: "https://sv443.net/cdn/other/briljaboticon.png",  // the URL to the profile picture
        activities: [                                              // all random activities - type can be "LISTENING", "PLAYING", "STREAMING", "WATCHING" - URL attribute of the status can be set to empty string or null to disable
            {
                activity: "Abusing the Non-Mods",
                type: "PLAYING",
                url: null
            },
            {
                activity: "Pruning all the newcomers",
                type: "PLAYING",
                url: null
            },
            {
                activity: "Kissing your mother with that mouth",
                type: "PLAYING",
                url: null
            },
            {
                activity: "Pinging @everyone",
                type: "PLAYING",
                url: null
            },
            {
                activity: "Banning @everyone",
                type: "PLAYING",
                url: null
            },
            {
                activity: "Secretly overthrowing the admins",
                type: "PLAYING",
                url: null
            },
            {
                activity: "Throwing hundreds of errors",
                type: "PLAYING",
                url: null
            },
            {
                activity: "Spreading love <3",
                type: "PLAYING",
                url: null
            },
            {
                activity: "the admins shower",
                type: "WATCHING",
                url: null
            },
            {
                activity: "to the souls of the damned",
                type: "LISTENING",
                url: null
            }
        ],
        changeInterval: 10,                                        // in min - the interval at which a new activity will be randomly chosen
    },

    // the wrapping script that enables the bot to be restarted intentionally and on a crash
    wrapper: {
        bootLoopDetection: 4000,        // in ms - how long it usually takes for the bot to boot up *completely* - this is used to detect a boot loop
        logInfo: true,                  // whether to log the wrapper status to the console (e.g. "[node-wrap]: Started child process")
        logFile: "./logs/wrapper.log",  // where the wrapper's log is located at (path relative to root)
    },

    // main command and event handler (the same one "mainFile" refers to)
    handler: {
        commandsBasePath: "./commands/",  // base path to the commands folder (path relative to root)
        eventsBasePath: "./events/",      // base path to the events folder (path relative to root)
    },

    // everything that's got something to do with logging
    logger: {
        basePath: "./logs/",  // path to the folder where all logs should be put (path relative to root)
        timestamp: true,      // whether or not a timestamp should be included in the logged lines
    },

    // everything that's external in some way
    external: {
        emojiServerID: "528629111904534534",  // the ID of the emoji server
        rolesFile: "./data/roles.json",       // the path to the file that contains all roles and their respective emoji IDs (path relative to root)
    },

    // the error catcher
    errorCatcher: {
        logChannelID: "578272728721653790",  // ID of the log channel
    },

    // permission related stuff
    perms: {
        // permission levels of certain roles - this is the same number that's used in a command's info object
        // array items have to be the roles' ID's
        // this array is essentially one-indexed so the first entry is perm level 1, the second is perm level 2 and so on
        // members that don't match at least one of these roles will have the perm level 0
        levels: [
            "578276358807879685",   // = 1
            "583793348523982899"    // = 2
        ]
    },

    // default RichEmbed settings
    richEmbed: {
        // RichEmbed color codes (use CSS hex color codes (e.g. "#abcdef"))
        colors: {
            error: "#ff0505",      // hex color code of very important RichEmbeds (stuff like errors)
            warn: "#ffd12b",       // hex color code of warning / info RichEmbeds (stuff like warnings or semi-important infos)
            default: "#49bcff",    // hex color code of normal / non-important RichEmbeds (stuff like responses to user commands)
            good: "#18ff0c"        // hex color code of good / successful RichEmbeds
        },

        // the default footer of RichEmbeds
        footer: {
            text: "© JabrilsCommunity 2019",
            icon: ""
        }
    },

    // user engagement stuff
    userEngagement: {
        toxicResponses: [
            "{{USER}}, do you kiss your mother with that mouth?",
            "{{USER}}, \\*beep boop\\* I detect bad word \\*beep boop\\*"
        ],
        newcomers: {
            newcomerRoleID: "578276364709265408"
        }
    }
}